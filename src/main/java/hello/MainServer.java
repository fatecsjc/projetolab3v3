package hello;

import static spark.Spark.*;

public class MainServer {

	final static Model model = new Model();

	public static void main(String[] args) {

		// Get port config of heroku on environment variable
		ProcessBuilder process = new ProcessBuilder();
		Integer port;
		if (process.environment().get("PORT") != null) {
			port = Integer.parseInt(process.environment().get("PORT"));
		} else {
			port = 8080;
		}
		port(port);
		
		initializeModel();
		
		staticFileLocation("/static");
		
		REST controller = new REST(model);
		controller.getLogin();
		controller.getExams();
		controller.getExam();
		controller.getNextQuestion();
		controller.setUserAnswer();
		controller.getActivityResults();
		controller.checkActivity();
		controller.resetActivity();
		controller.createUser();
	}
	
	public static void initializeModel() {
		
		// Add initial users
		model.addUser(new User("Nicholas", true, new Login("nicholas.checan@gmail.com", "123")));
		model.addUser(new User("Leonardo", true, new Login("lcp.leonardo@outlook.com", "123")));
		model.addUser(new User("Gustavo", false, new Login("luisaraki@hotmail.com", "123")));
		model.addUser(new User("William", true, new Login("wifosilv@gmail.com", "123")));
		model.addUser(new User("Robson", true, new Login("robsoncartes@gmail.com", "123")));
		
		// Add exams and questions
		Exam newExam;
		Question newQuestion;
		
		newExam = new Exam("Database Test", "Technology", "Database questions to improve your knowledge", "Nicholas");
		
		newQuestion = new Question("You can add a row using SQL in a database with which of the following?");
		newQuestion.addAnswer(new Answer("ADD", false));
		newQuestion.addAnswer(new Answer("CREATE", false));
		newQuestion.addAnswer(new Answer("INSERT", true));
		newQuestion.addAnswer(new Answer("MAKE", false));
		newExam.addQuestion(newQuestion);
		
		newQuestion = new Question("The SQL WHERE clause:");
		newQuestion.addAnswer(new Answer("limits the column data that are returned.", false));
		newQuestion.addAnswer(new Answer("limits the row data are returned.", true));
		newQuestion.addAnswer(new Answer("Both A and B are correct.", false));
		newQuestion.addAnswer(new Answer("Neither A nor B are correct.", false));
		newExam.addQuestion(newQuestion);
		
		newQuestion = new Question("Which of the following is the original purpose of SQL?");
		newQuestion.addAnswer(new Answer("To specify the syntax and semantics of SQL data definition language", false));
		newQuestion.addAnswer(new Answer("To specify the syntax and semantics of SQL manipulation language", false));
		newQuestion.addAnswer(new Answer("To define the data structures", false));
		newQuestion.addAnswer(new Answer("All of the above.", true));
		newExam.addQuestion(newQuestion);
		
		model.addExam(newExam);
		
		newExam = new Exam("General Knowledge", "General", "Some wikipedia questions for you to enjoy!", "Gustavo");
		
		newQuestion = new Question("If a=1 and b=2, what is a+b?");
		newQuestion.addAnswer(new Answer("12", false));
		newQuestion.addAnswer(new Answer("3", true));
		newQuestion.addAnswer(new Answer("4", false));
		newQuestion.addAnswer(new Answer("10", false));
		newExam.addQuestion(newQuestion);
		
		newQuestion = new Question("The IT capital of India is:");
		newQuestion.addAnswer(new Answer("Bangalore", true));
		newQuestion.addAnswer(new Answer("Mumbai", false));
		newQuestion.addAnswer(new Answer("Mexico", false));
		newQuestion.addAnswer(new Answer("Hyderabad", false));
		newExam.addQuestion(newQuestion);
		
		model.addExam(newExam);
		
		// Robson Exam
		/*
		newExam = new Exam("Advanced PL/SQL", "General PL/SQL", "Other practical questions", "Robson");
		
		newQuestion = new Question("How many types of PL/SQL loops are exists?");
		newQuestion.addAnswer(new Answer("3", true));
		newQuestion.addAnswer(new Answer("4", false));
		newQuestion.addAnswer(new Answer("2", false));
		newQuestion.addAnswer(new Answer("1", false));
		newExam.addQuestion(newQuestion);

		model.addExam(newExam);
		*/

		newExam = new Exam("Android Quis", "Android Basic Test", "Online Android Quis", "Robson");

		newQuestion = new Question("Every Android application must have the file AndroidManifest.xml file in its root " +
				"directory. Select one:");
		newQuestion.addAnswer(new Answer("True", true));
		newQuestion.addAnswer(new Answer("False", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("Which among the following is not a member of Open Handset Alliance. Select one");
		newQuestion.addAnswer(new Answer("Apple", true));
		newQuestion.addAnswer(new Answer("Wipro", false));
		newQuestion.addAnswer(new Answer("None of those", false));
		newQuestion.addAnswer(new Answer("Samsung", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("android:maxSdkVersion in AndroidManifest.xml is not recommended.");
		newQuestion.addAnswer(new Answer("True", true));
		newQuestion.addAnswer(new Answer("False", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("Android is a linux based operating system. Select one:");
		newQuestion.addAnswer(new Answer("True", true));
		newQuestion.addAnswer(new Answer("False", true));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("What is the use of AndroidManifest.xml file?");
		newQuestion.addAnswer(new Answer("It describes the components of the application.", false));
		newQuestion.addAnswer(new Answer("It declares the minimum level of the Android API that the application requires.", false));
		newQuestion.addAnswer(new Answer("All", true));
		newQuestion.addAnswer(new Answer("It facilitates to provide a unique name for the application by specifying package name.", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("The root element of AndroidManifest.xml is. Select one:");
		newQuestion.addAnswer(new Answer("application", false));
		newQuestion.addAnswer(new Answer("manifest", true));
		newQuestion.addAnswer(new Answer("activity", false));
		newQuestion.addAnswer(new Answer("action", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("It is not possible to create a user interface with out XML layout file. Select one:");
		newQuestion.addAnswer(new Answer("True", true));
		newQuestion.addAnswer(new Answer("False", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("In Android, an XML layout file is used to design the contents of the user interface screen. Select one:");
		newQuestion.addAnswer(new Answer("True", true));
		newQuestion.addAnswer(new Answer("False", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("What is the name of the class which is inherited to create a user interface screen? Select one:");
		newQuestion.addAnswer(new Answer("ViewGroup", false));
		newQuestion.addAnswer(new Answer("View", false));
		newQuestion.addAnswer(new Answer("Activity", true));
		newQuestion.addAnswer(new Answer("None of these", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("The basic building element of Android's user interface is called:");
		newQuestion.addAnswer(new Answer("View", true));
		newQuestion.addAnswer(new Answer("ContentProvider", false));
		newQuestion.addAnswer(new Answer("ViewGroup", false));
		newQuestion.addAnswer(new Answer("Layout", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("Specify the directory name where the XML layout file are stored. Select one:");
		newQuestion.addAnswer(new Answer("/assets", false));
		newQuestion.addAnswer(new Answer("/src", false));
		newQuestion.addAnswer(new Answer("/res/values", false));
		newQuestion.addAnswer(new Answer("/res/layout", true));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("Android is initially developed by. Select one:");
		newQuestion.addAnswer(new Answer("Google Inc", true));
		newQuestion.addAnswer(new Answer("Apple Inc", false));
		newQuestion.addAnswer(new Answer("Open Handset Alliance", false));
		newQuestion.addAnswer(new Answer("Android Inc", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("________ represents a single user interface screen. Select one:");
		newQuestion.addAnswer(new Answer("Broadcast Reveiver", false));
		newQuestion.addAnswer(new Answer("Activity", true));
		newQuestion.addAnswer(new Answer("Service", false));
		newQuestion.addAnswer(new Answer("Content Provider", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("Is used to uniquely identify the framework API revision offered by a version of the Android Plataform. Select one:");
		newQuestion.addAnswer(new Answer("Version Number", true));
		newQuestion.addAnswer(new Answer("Code Name", false));
		newQuestion.addAnswer(new Answer("API Level", false));
		newExam.addQuestion(newQuestion);

		newQuestion = new Question("Which attribute of the element <uses-sdk> is used to specify the minimum API Level required for the application to run?");
		newQuestion.addAnswer(new Answer("android:targetSdkVersion", false));
		newQuestion.addAnswer(new Answer("android:maxSdkVersion", false));
		newQuestion.addAnswer(new Answer("android:minSdkVersion", true));
		newExam.addQuestion(newQuestion);

		model.addExam(newExam);
	}
}
